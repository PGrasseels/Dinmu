$(function () {

    pageUnits = 'px';
    colUnits = 'px';
    pagewidth = 960;
    columns = 12;
    columnwidth = 70;
    gutterwidth = 12;
    pagetopmargin = 35;
    rowheight = 20;
    gridonload = 'off';
    makehugrid();
    setgridonload();

    $('#GridToggle').on('click', function () {
        window.hugrid.toggleState();

        event.preventDefault();
    });

    $('#main-nav img').on('click', function () {
        $('html, body').animate({
            scrollTop: 0
        }, 750);
    });

    $('.box').children('article').hide();

    $('#main-nav').scrollToFixed();

    $(window).scroll(function () {
        $('.link').removeClass('active');
        if ($(window).scrollTop() > 3100) {
            $('#reasonsLink').addClass('active');
        } else {
            if ($(window).scrollTop() > 2490) {
                $('#workLink').addClass('active');
            } else {
                $('#aboutLink').addClass('active');
            }
        }
    });

    $('#main-nav ul li a.link').click(function () {
        event.preventDefault();
        $('html, body').animate({
            scrollTop: $($.attr(this, 'href')).offset().top
        }, 750);
    });

    $('#contactShow').on('click', function () {
        $('#contact').slideDown(500);
        $('#contactContent').show(0);
    });

    $('#contact .close').on('click', function () {
        $('#contact').slideUp(500);
        $('#detailsContent').slideUp(500).delay(250);
    });

    $('#giveMore').on('click', function () {

        $('.box').children('article').slideToggle();

        if ($(this).attr('data-id') == 'Details') {
            $(this).html('Complete a form').attr('data-id', 'Contact');
        } else {
            $(this).html('My details').attr('data-id', 'Details');
        }
    });

    $('#privacy').on('click', function () {
        event.preventDefault();
        $('#policy').slideToggle();
    });

    $('#policy').on('click', function () {
        event.preventDefault();
        $('#policy').slideToggle();
    });

    $('#postContact').on('click', function () {
        event.preventDefault();

        var dataObject = {
            name: $('#contactName').val(),
            email: $('#contactEmail').val(),
            content: $('#contactTextarea').val()
        };

        if (dataObject.name == '' || dataObject.email == '' || dataObject.content == '') {
            $('.error').show(0);
            $('.box').css('height', 660);
        } else {
            $('.error').hide(0);
            $('.box').css('height', 622);
            $.ajax({
                type: 'POST',
                url: 'postForm.php',
                data: {
                    name: $('#contactName').val(),
                    email: $('#contactEmail').val(),
                    content: $('#contactContent').val()
                },
                success: function (data) {
                    if (data.success) {
                        alert('Your request has been posted.');
                    }
                }
            });
        }
    });
});