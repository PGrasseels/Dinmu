<?php

header('Content-type: application/json');

try
{
	$db = new PDO('mysql:host=localhost;dbname=loneska', 'root', 'puce0123');

	extract($_POST);

	$stmt = $db->prepare("INSERT INTO contact (name, email, content, dateContact) VALUES (:name, :email, :content, :dateContact)");
	$stmt->bindParam(':name', $name);
	$stmt->bindParam(':email', $email);
	$stmt->bindParam(':content', $content);
	$stmt->bindParam(':dateContact', Date('Y/m/d'));
	$stmt->execute();

	echo json_encode(array( 'success' => true));

}

catch(Exception $e)
{
	echo json_encode(array( 'success' => false, 'errorMessage' => $e->getMessage(), 'errorCode' => $e->getCode()));
}

?>