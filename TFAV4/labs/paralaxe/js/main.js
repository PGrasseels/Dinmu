(function($) {

	var $menu, $sections,$parallax,
		currentSection,link,sectionCount,sectionHeight,
		backgroundHeight,diffHeight;

	//--------------------------------------------------
	//j'arrête de déclarer des variables 
	//--------------------------------------------------

	currentSection = 0;
	$menu = $("nav a");
	$menu.click(gotoSection);
	$sections = $("section");
	$paralaxes = $("parallax");
	$scrollable = $(document);

	/*On va récuperer les valeurs dont on aura besoin */

	sectionCount = $sections.length;
	//récupère la hauteur des sections sur dase de la hauteur de la première
	sectionHeight = $sections.first().height();
	//récupère la hauteur des backgrounds sur dase de la hauteur de la première
	backgroundHeight = $sections.first().find(".background").height();//700
	//calculer la différence des deux
	diffHeight = backgroundHeight - sectionHeight; //700-500=200

	function scrollTo(section,speed) {
		$("html,body").stop().animate({
			scrollTop: $(section).offset().top
		}, speed);
		// body...
	}

	
	


	function gotoSection(event){


		//récupère l'attribut href de l'élément cliqué
		link = $(this).attr("href");

		scrollTo(link, 250)
		return false;
	}

	$(window).on("keyup", keyPressed);

	function keyPressed(event) {
		var keyCode = event.keyCode;
		var $item;

		if(keyCode == 38){

			if(currentSection > 0) {
			currentSection--;
			

			}

			
		}
		//flêche bas
		if(keyCode == 40){

			if(currentSection < $menu.length-1) {
			currentSection++;

			}
			
		}
		//récupere l'item dans le menu
		$item = $($menu[currentSection]);
		//click sur le lien
		$item.trigger("click");
		return false;
	}

	$scrollable.scroll(update);

	var currentScroll,scroll;

	function update(event) {

			//event.target.bodyscrollTop

			currentScroll 
			= $scrollable.scrollTop();

			for(var i = 0;i < sectionCount; i++ ){

				sectionOffset = $sections.eq(i).offset().top;
				

				scroll = (currentScroll - sectionOffset) / sectionHeight * diffHeight;

				$paralaxes.eq(i).css("top",scroll+"px");

			}
	}


}	)	(jQuery);

