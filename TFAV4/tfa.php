﻿<!doctype html>
<html>
<head>
    <meta charset="UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title>Laura Aït-Braham</title>
    <meta name="description" content="Responsive Animated Border Menus with CSS Transitions" />
    <meta name="keywords" content="navigation, menu, responsive, border, overlay, css transition" />
    <meta name="author" content="Laura Aït-Braham" />
    <link rel="shortcut icon" href="favicon.ico" />
    <link rel="stylesheet" type="text/css" href="css/tfadesign.css" />
    <link rel="stylesheet" type="text/css" href="css/design.css" />
    <link rel="stylesheet" type="text/css" href="css/hugrid.css" />

  <!--<script>
        (function (i, s, o, g, r, a, m) {
            i['GoogleAnalyticsObject'] = r; i[r] = i[r] || function () {
                (i[r].q = i[r].q || []).push(arguments)
            }, i[r].l = 1 * new Date(); a = s.createElement(o),
            m = s.getElementsByTagName(o)[0]; a.async = 1; a.src = g; m.parentNode.insertBefore(a, m)
        })(window, document, 'script', '//www.google-analytics.com/analytics.js', 'ga');

        ga('create', 'UA-51811151-1', 'loneska.be');
        ga('send', 'pageview');

    </script>-->
    <script src="js/modernizr.custom.js"></script>
</head>
<body>
    <div class="social">
        <ul>
            <li><a href="https://www.linkedin.com/in/loneska" target="_blank"><div class="linkedin"></div></a></li>
            <li><a href="https://www.behance.net/Loneska" target="_blank"><div class="behance"></div></a></li>
            <li><a href="http://www.pinterest.com/Loneska/" target="_blank"><div class="pinterest"></div></a></li>
            <li><a href="https://twitter.com/Loneska" target="_blank"><div class="twitter"></div></a></li>
            <li><a href="" target="_blank"><div class="facebook"></div></a></li>
        </ul>
    </div>
    <?php include('./include/intro.php'); ?>
    <section>
        <nav id="main-nav">
          <img src="./img/logo.png" alt="Logo"/>
            <ul>
                <li><a href="#about" class="link" id="aboutLink">I'm Loneska</a></li>
                <li><a href="#works" class="link" id="workLink" >I show you what I do</a></li>
                <li><a href="#reasons" class="link" id="reasonsLink" >and why I could be valuable</a></li>
                <li><a id="contactShow" class="contactShow">feel free to contact me</a></li>
            </ul>
        </nav>
        <section class="about white" id="about">
            <h1>WHO I AM</h1>
            <h2>I'm Laura Aït-braham, also known as Loneska, here are some piece of me</h2>
            <div class="underline"></div>
            <div class="wrap">
                <aside>
                    <div class="icon icon-degree">
                    </div>
                    <p>
                        I'm a french student in <span class="white">webdesign</span> who lives in a great country called Belgium.
                        After a first year in infographic skills and another in 3D graphics, I'm currently studying <a href="http://dwm.re/" target="_blank" class="corail">desgin web and multimedia</a> at <a href="http://www.infographie-sup.be/" target="_blank" class="corail">ESIAJ</a>, Namur.
                    </p>
                    <p>
                        I'm looking forward a <span class="white">3 months internship starting in october 2014</span> in a friendly web agency where I could improve myself in design and development. I'm dying to discover new places, teamworks, challenges. I bet I can be a sweet trainee, <a href="#" class="corail contactShow">bet with me?</a>
                    </p>
                    <a href="./download/resumeofaitbrahamlaura.pdf" target="blank" class="btn" style="margin-top:20px">Take a look at my resume</a>
                    <p>
                        <a href="#reasons" class="corail">Not convinced yet?</a>
                    </p>
                </aside>
                <article>
                    <p>
                        Since I was a little girl, I wanted to become an adventurous archeologist, a space traveler and a wizard. As you may guess, my plans didn't turned exactly the way I expected but in the end I feel lucky because <strong>I found a way for those who seek to find a way to leave their world behind.</strong> The virtual world is not just a domain in which I'm good at, it's a real passion, I felt in love with it.
                    </p>
                    <p>
                        When I works, I feel like an archeologist which explore some unknown place, like a space traveler because I try to always improve my tools in order to go farest, and also like a wizard because there's some kind of magic here. For me, it's really important to create friendly-user interfaces which allows friendly-user experiences.
                    </p>
                    <blockquote><span class="corail">People will forget what you did, people will forget what you said. But people will never forget how you made them feel"</span> -From Maya Angelou, it express perfectly what I think design is</blockquote>
                    <p>
                        When I'm not working on a project I like to spend my time by reading, planning trips, laying on the grass and observ the clouds, spend time with my two lovely daughters, Lily and Jade.
                    </p>
                    <p>
                        Now that you know better who I am and for those who are not afraid, I propose you to see some of my works <a href="#works" class="corail">down below.</a>
                    </p>
                </article>
                <div style="clear:both"></div>
                <div class="row">
                    <div class="col-md-3">
                        <img src="img/travel.png" class="icon-md hover-details" alt="" />
                        <p>I wish I could travel around the world.</p>
                    </div>
                    <div class="col-md-3">
                        <img src="img/photo.png" class="icon-md hover-details" alt="" />
                        <p>My reflex to take photographies comes from my grandfather.</p>
                    </div>
                    <div class="col-md-3">
                        <img src="img/nature.png" class="icon-md hover-details" alt="" />
                        <p>I really love and respect Nature.</p>
                    </div>
                    <div class="col-md-3">
                        <img src="img/space.png" class="icon-md hover-details" alt="" />
                        <p>My need to read comes from my mother.</p>
                    </div>
                </div>
            </div>
        </section>
        <section class="works grey" id="works" style="padding-top:80px;">
            <div class="wrap">
                <h1>WHAT I DO</h1>
                <h2>Here are several works I baked with love</h2>
                <div class="row">
                    <div class="col-md-6">
                        <article class="work-multi">
                            <div class="pic-wrap">
                                <aside>Hollywoodisme</aside>
                                <img src="./img/sharesomething.jpg" alt="Alternate Text" />
                                <a href="#">See the project</a>
                            </div>
                            <div class="pic-desc">
                                <header>
                                    <h3>Ever wonder about Hollywood?</h3>
                                </header>
                                <p>Have you any idea of the women's place in Hollywood? Do you think there is an improvement? If you have any interest, feel free to check this datavisualisation</p>
                            </div>
                        </article>
                    </div>
                    <div class="col-md-6">
                        <article class="work-multi right">
                            <div class="pic-wrap">
                                <aside>Change the world with the web</aside>
                                <img src="./img/hollywoodisme.png" alt="Alternate Text" />
                                <a href="#">How it was made</a>
                            </div>
                            <div class="pic-desc">
                                <header>
                                    <h3>Did you want to save the world?</h3>
                                </header>
                                <p>First of all, thank you, you have a beautiful mind and I wish you good luck. I think the web can help you and I explain why. So why not take a look before you start?</p>
                            </div>
                        </article>
                    </div>
                </div>
                <article class="work-single">
                    <div class="pic-wrap">
                        <aside>Share something</aside>
                        <img src="./img/save.jpg" alt="Alternate Text" />
                        <a href="#">See the prototype</a>
                    </div>
                    <div class="pic-desc">
                        <header>
                            <h3>Anything to share?</h3>
                        </header>
                        <p>Nowadays, we're all connected. We feel the need to share with each other to exist. How many tweets, status did you publish per day? What about an application to post your last event on your favorite social network at the same time without going on them?</p>
                        <ul>
                            <li>Lorem dolor si anet</li>
                            <li>Lorem dolor si anet</li>
                            <li>Lorem dolor si anet</li>
                            <li>Lorem dolor si anet</li>
                        </ul>
                    </div>
                </article>
            </div>
        </section>
        <section class="reasons white" id="reasons">
            <h1>WHY YOU SHOULD HIRE ME</h1>
            <h2>Here are some reasons why I'm valuable</h2>
            <div class="underline"></div>
            <div class="row" style="margin-top:80px">
                <div class="col-md-4">
                    <div class="col-content">
                        <img src="./img/learn.png" />
                        <h2>I enjoy learning</h2>
                        <p>I feel the need to go farest and deepest.</p>
                        <p>Discovering new tools, new possibilities amaze me.</p>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="col-content">
                        <img src="./img/love.png" />
                        <h2>I love what I do</h2>
                        <p>Creating and sharing is not just a work, it's what I love</p>
                        <p>I'm really motivated but also completely passionnate by what I do</p>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="col-content">
                        <img src="./img/adaptative.png" />
                        <h2>I am very adaptative</h2>
                        <p>I can handle several personalities and levels</p>
                        <p>Discover news workplaces and teammates don't afraid me</p>
                    </div>
                </div>
            </div>
        </section>
        <?php include('./include/modal.php'); ?>
    </section>
    <?php include('./include/footer.php'); ?>
    <script src="js/jquery.js"></script>
    <script type="text/javascript" src="js/jquery-ui-1.10.4.js"></script>
    <script type="text/javascript" src="js/jquery-scrolltofixed.js"></script>
    <script type="text/javascript" src="js/jquery.easing.1.3.js"></script>
    <script type="text/javascript" src="js/jquery.animate-enhanced.min.js"></script>
    <script src="js/hugrid.js"></script>  
    <script type="text/javascript" src="js/script.js"></script>
</body>
</html>