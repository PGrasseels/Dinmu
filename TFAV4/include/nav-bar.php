        <nav id="main-nav">
            <ul>
                <li><a href="#about" class="link" id="aboutLink">I'm Loneska</a></li>
                <li><a href="#works" class="link" id="workLink">I show you what I do</a></li>
                <li><a href="#reasons" class="link" id="reasonsLink">and why I could be valuable</a></li>
                <li><a id="contactShow">Feel free to contact me</a></li>
            </ul>
        </nav>