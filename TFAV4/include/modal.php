<section class="contact" id="contact">
            <div class="box absolute-center">
                <article id="contactContent">
                    <div class="close"></div>
                    <h1 style="padding-top:20px;">KEEP IN TOUCH</h1>
                    <h2 style="font-size:1.2em;">What about a first contact between us?</h2>
                    <div class="row">
                        <div class="col-md-12">
                            <form>
                                <div class="form-icon">
                                    <input type="text" placeholder="What's your lovely name ?" id="contactName" />
                                </div>
                                <div class="form-icon">
                                    <input type="email" placeholder="An email adress to recontact you ?" id="contactEmail" />
                                </div>
                                <div class="form-icon">
                                    <textarea placeholder="What do you want to tell me ?" id="contactTextarea"></textarea>
                                </div>
                                <div class="error">
                                    Oups, some field are empty, to contact me, you must fill all fields
                                </div>
                                <input type="submit" class="btn" value="Send" id="postContact">
                            </form>
                        </div>
                    </div>
                </article>
                <article id="detailsContent">
                    <div class="close"></div>
                    <h1 style="padding-top:20px;">MY DETAILS</h1>
                    <h2 style="font-size:1.2em;">Feel free to use them, I don't bite</h2>
                    <img src="img/email.png" class="icon-sm hover-details" alt="">
                    <p>Write me a lovely email</p>
                    <p>laura@loneska.be</p>
                    <img src="img/smartphone.png" class="icon-sm hover-details" alt="">
                    <p>Please, if it's 3am, don't</p>
                    <p>0491/22 90 50</p>
                    <img src="img/cofee.png" class="icon-sm hover-details" alt="">
                    <p>Take a cofee together</p>
                    <p>Namur, Belgium</p>
                </article>
                <div style="clear:both"></div>
                <footer>
                    <button id="giveMore" data-id="Details">My Details</button>
                </footer>
            </div>
        </section>
        <section id="policy">
            <div class="wrap">
                <article>
                    <h2>Privacy Policy</h2>
                    <p>I should inform you that I use <a href="" class="corail">Google Analytics</a> but please don't panic or feel stalked, I do that for you. It allows me to know what kind of navigator you use, your screen size, from where you are and how you come on this site. With all this informations I will be able to improve the site in order to offer you the most optimal user experience as possible.</p>
                    <p>Anyway, if you still feel worried about this, be aware that there is many ways to desable this process. Feel free, to do so.</p>
                </article>
            </div>
        </section>